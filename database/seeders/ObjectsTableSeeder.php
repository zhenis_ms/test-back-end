<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ObjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('objects')->insert(
            [
                [
                    'id' => 1,
                    'parent_id' => 0,
                    'name' => 'A1',
                ],
                [
                    'id' => 2,
                    'parent_id' => 0,
                    'name' => 'B1',
                ],
                [
                    'id' => 3,
                    'parent_id' => 0,
                    'name' => 'C1',
                ],
                [
                    'id' => 4,
                    'parent_id' => 1,
                    'name' => 'A1.1',
                ],
                [
                    'id' => 5,
                    'parent_id' => 1,
                    'name' => 'A1.2',
                ],
                [
                    'id' => 6,
                    'parent_id' => 1,
                    'name' => 'A1.3',
                ],
                [
                    'id' => 7,
                    'parent_id' => 1,
                    'name' => 'A1.4',
                ],
                [
                    'id' => 8,
                    'parent_id' => 4,
                    'name' => 'A1.1.1',
                ],
                [
                    'id' => 9,
                    'parent_id' => 4,
                    'name' => 'A1.1.2',
                ],
                [
                    'id' => 10,
                    'parent_id' => 4,
                    'name' => 'A1.1.3',
                ],
                [
                    'id' => 11,
                    'parent_id' => 7,
                    'name' => 'A1.4.1',
                ],
                [
                    'id' => 12,
                    'parent_id' => 7,
                    'name' => 'A1.4.2',
                ],
                [
                    'id' => 13,
                    'parent_id' => 2,
                    'name' => 'B1.1',
                ],
                [
                    'id' => 14,
                    'parent_id' => 2,
                    'name' => 'B1.2',
                ],
                [
                    'id' => 15,
                    'parent_id' => 2,
                    'name' => 'B1.3',
                ],
                [
                    'id' => 16,
                    'parent_id' => 2,
                    'name' => 'B1.4',
                ],
                [
                    'id' => 17,
                    'parent_id' => 15,
                    'name' => 'B1.3.1',
                ],
                [
                    'id' => 18,
                    'parent_id' => 15,
                    'name' => 'B1.3.2',
                ],
                [
                    'id' => 19,
                    'parent_id' => 15,
                    'name' => 'B1.3.3',
                ],
                [
                    'id' => 20,
                    'parent_id' => 16,
                    'name' => 'B1.4.1',
                ],
                [
                    'id' => 21,
                    'parent_id' => 16,
                    'name' => 'B1.4.2',
                ],
                [
                    'id' => 22,
                    'parent_id' => 3,
                    'name' => 'C1.1',
                ],
                [
                    'id' => 23,
                    'parent_id' => 3,
                    'name' => 'C1.2',
                ],
                [
                    'id' => 24,
                    'parent_id' => 23,
                    'name' => 'C1.2.1',
                ],
                [
                    'id' => 25,
                    'parent_id' => 23,
                    'name' => 'C1.2.2',
                ],
            ],
        );
    }
}
