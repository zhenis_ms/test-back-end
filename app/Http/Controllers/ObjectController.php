<?php

namespace App\Http\Controllers;

use App\Http\Requests\ObjectsRequest;
use App\Models\ObjectModel;
use Illuminate\Http\Request;
use App\Services\ObjectsService;

class ObjectController extends Controller
{
    public function index(Request $request)
    {
        $data = $request->all();

        return response()->json((new ObjectsService($data))->getObjects());
    }

    public function wood(Request $request)
    {
        $data = $request->all();
//        $data = ObjectModel::all()->toArray();

        return response()->json((new ObjectsService($data))->getWoods());
    }
}
