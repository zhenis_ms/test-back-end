<?php


namespace App\Services;


use App\Models\ObjectModel;

class ObjectsService
{
    public $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getObjects()
    {
        $objects = ObjectModel::with('children')->where('id', $this->data['id'])->get();

        return $objects;
    }

    public function getWoods()
    {
        $objects = $this->data;

        $tree = $this->buildTree($objects);

        return $tree;
    }

    private function buildTree(array $elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $parent = $this->buildTree($elements, $element['id']);
                if ($parent) {
                    $element['parent'] = $parent;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

}
