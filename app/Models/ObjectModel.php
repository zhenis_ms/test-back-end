<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ObjectModel extends Model
{
    protected $table = 'objects';

    protected $fillable = [
        'id', 'parent_id', 'name'
    ];

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
}
